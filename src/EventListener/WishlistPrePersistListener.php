<?php

namespace App\EventListener;

use App\Entity\Wishlist;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class WishlistPrePersistListener
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getObject();
        if (!$entity instanceof Wishlist) {
            return;
        }
        $entity->fixUser($this->tokenStorage->getToken()->getUser());
    }
}
