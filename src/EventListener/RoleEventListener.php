<?php


namespace App\EventListener;

use App\Entity\Role;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;

class RoleEventListener implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'onFlush',
        );
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach (
            $uow->getScheduledCollectionUpdates() as $collection
        ) {
            if ($collection->getOwner() instanceof Role) {
                $users = $collection->getOwner()->getUsers();
                foreach ($users as $user) {
                    $user->setValidAfter(new \DateTime());
                    $classMetadata = $em->getClassMetadata('App\Entity\User');
                    $uow->computeChangeSet($classMetadata, $user);
                }
            }
        }
    }
}
