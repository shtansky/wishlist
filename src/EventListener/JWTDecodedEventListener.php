<?php


namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;

class JWTDecodedEventListener
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param JWTDecodedEvent $event
     *
     * @throws \Exception
     *
     * @return void
     */
    public function onJWTDecoded(JWTDecodedEvent $event)
    {
        $payload = $event->getPayload();

        /**
         * @var $user User
         */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(
                [
                    'username' => $payload['username']
                ]
            )
        ;
        if ($user && !is_null(
                $user->getValidAfter()
            ) && $payload['iat'] < $user->getValidAfter()->getTimestamp()) {
            $event->markAsInvalid();
        }
    }
}
