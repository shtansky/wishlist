<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class WishExportCommand extends Command
{
    protected static $defaultName = 'wish:export';

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct(self::$defaultName);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Exports the wishlists of all users to CSV')
            ->addArgument(
                'filename',
                InputArgument::OPTIONAL,
                'Filename of exported CSV'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $io = new SymfonyStyle($input, $output);
        $filename = $input->getArgument('filename');

        if ($filename) {
            $io->note(sprintf('Exporting to: %s', $filename));
        } else {
            $filename = date('Y-m-d') . '.csv';
            $io->note(sprintf('Exporting to default location: %s', $filename));
        }

        $conn = $this->entityManager->getConnection();
        $sql  = <<<EOD
SELECT `u`.`name` as `user`, `w`.`title` as `wishlist`, COUNT(`i`.`id`) as `qty`
FROM `user` u
LEFT JOIN `wishlist` w ON w.user_id = u.id
LEFT JOIN `item` i ON i.wishlist_id = w.id
GROUP BY `user`, `wishlist`
ORDER BY `u`.`id` ASC
EOD;
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $csv = fopen(__DIR__ . "/../../public/csv/" . $filename, 'w+');
        foreach ($stmt->fetchAllAssociative() as $fields) {
            fputcsv($csv, $fields);
        }
        fclose($csv);

        $io->success(
            'Exported successfully. You may find the file at http://localhost/csv/'.$filename
        );

        return Command::SUCCESS;
    }
}
