<?php

namespace App\Tests\Entity;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Item;

class ItemTest extends ApiTestCase
{
    protected $stack = [];
    protected static $item;

    public function setUp(): void
    {
        $response             = static::createClient()->request(
            'POST',
            '/login_jwt',
            [
                'json' =>
                    [
                        'username' => 'nicos',
                        'password' => '123456'
                    ]
            ]
        );
        $this->stack['token'] =
            (json_decode($response->getContent(), true))['token'];
    }

    public function testItemApiIsProtected(): void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/items'
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testGetItemCollection(): void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/items',
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceCollectionJsonSchema(Item::class);
    }

    public function testCreateItem(): void
    {
        $response = static::createClient()->request(
            'POST',
            '/api/items',
            [
                'json' => [
                    'title' => 'Test Item',
                    'wishlist' => '/api/wishlists/1'
                ],
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertMatchesResourceItemJsonSchema(Item::class);
        $this->assertContains('"title":"Test Item"', $response->getContent());
        self::$item = (json_decode($response->getContent(), true))['@id'];
    }

    public function testGetItem(): void
    {
        $response = static::createClient()->request(
            'GET',
            self::$item,
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceItemJsonSchema(Item::class);
        $this->assertContains('"title":"Test Item"', $response->getContent());
    }

    public function testUpdateItem(): void
    {
        $response = static::createClient()->request(
            'PATCH',
            self::$item,
            [
                'json' => [
                    'title' => 'New Title'
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceItemJsonSchema(Item::class);
        $this->assertContains('"title":"New Title"', $response->getContent());
    }
    public function testDeleteItem(): void
    {
        $response = static::createClient()->request(
            'DELETE',
            self::$item,
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(204, $response->getStatusCode());
    }
}
