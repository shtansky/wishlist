<?php

namespace App\Tests\Entity;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Wishlist;

class WishlistTest extends ApiTestCase
{
    protected $stack = [];
    protected static $wishlist;

    public function setUp(): void
    {
        $response             = static::createClient()->request(
            'POST',
            '/login_jwt',
            [
                'json' =>
                    [
                        'username' => 'nicos',
                        'password' => '123456'
                    ]
            ]
        );
        $this->stack['token'] =
            (json_decode($response->getContent(), true))['token'];
    }

    public function testWishlistApiIsProtected(): void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/wishlists'
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testGetWishlistCollection(): void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/wishlists',
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceCollectionJsonSchema(Wishlist::class);
    }

    public function testCreatewishlist(): void
    {
        $response = static::createClient()->request(
            'POST',
            '/api/wishlists',
            [
                'json' => [
                    'title' => 'Test Wishlist'
                ],
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertMatchesResourceItemJsonSchema(Wishlist::class);
        $this->assertContains('"title":"Test Wishlist"', $response->getContent());
        self::$wishlist = (json_decode($response->getContent(), true))['@id'];
    }

    public function testGetWishlist(): void
    {
        $response = static::createClient()->request(
            'GET',
            self::$wishlist,
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceItemJsonSchema(Wishlist::class);
        $this->assertContains('"title":"Test Wishlist"', $response->getContent());
    }

    public function testUpdateWishlist(): void
    {
        $response = static::createClient()->request(
            'PATCH',
            self::$wishlist,
            [
                'json' => [
                    'title' => 'New Title'
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceItemJsonSchema(Wishlist::class);
        $this->assertContains('"title":"New Title"', $response->getContent());
    }
    public function testDeleteWishlist(): void
    {
        $response = static::createClient()->request(
            'DELETE',
            self::$wishlist,
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(204, $response->getStatusCode());
    }
}
