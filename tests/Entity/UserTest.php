<?php

namespace App\Tests\Entity;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;

class UserTest extends ApiTestCase
{
    protected $stack = [];

    public function setUp() : void
    {
        $response = static::createClient()->request(
            'POST',
            '/login_jwt',
            ['json' =>
                [
                    'username' => 'nicos',
                    'password' => '123456'
                ]
            ]
        );
        $this->stack['token'] = (json_decode($response->getContent(), true))['token'];
    }

    public function testUserCantLoginWithWrongCredetials() : void
    {
        $response = static::createClient()->request(
            'POST',
            '/login_jwt',
            ['json' =>
                [
                    'username' => 'nicos',
                    'password' => 'abcdef'
                ]
            ]
        );
        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testUserCanLogin() : void
    {
        $response = static::createClient()->request(
            'POST',
            '/login_jwt',
            ['json' =>
                [
                    'username' => 'nicos',
                    'password' => '123456'
                ]
            ]
        );
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("token", $response->getContent());
    }

    public function testUserApiIsProtected() : void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/users'
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testGetUserCollection() : void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/users',
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer '. $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceCollectionJsonSchema(User::class);
    }

    public function testGetUser() : void
    {
        $response = static::createClient()->request(
            'GET',
            '/api/users/1',
            [
                'headers' => [
                    'Accept' => 'application/ld+json',
                    'Authorization' => 'Bearer '. $this->stack['token'],
                ],
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesResourceItemJsonSchema(User::class);
        $this->assertContains('"username":"nicos"', $response->getContent());
    }
}
